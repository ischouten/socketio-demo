import random
import signal
import logging
import asyncio
from webapp import message, app, web


log = logging.getLogger("logger")


def shutdown(future, loop):
	print("Received stop signal, cancelling tasks...")
	future.cancel()
	loop.stop()
	log.info("Program quit.")
	loop.close()


class Poller():
	def __init__(self):
		self.keys = ["item1", "item2", "item3"]

	async def poll_loop(self, i):
		value = random.randint(0, 10)

		payload = {self.keys[i%3]: value}

		i += 1
		await asyncio.sleep(1)
		await message(payload)
		await asyncio.ensure_future(self.poll_loop(i))


if __name__ == '__main__':

	poller = Poller()
	futures = []
	loop = asyncio.get_event_loop()

	coro = asyncio.ensure_future(poller.poll_loop(0), loop=loop)
	futures.append(coro)

	app['items'] = poller.keys
	coro = asyncio.ensure_future(web.run_app(app, port=8080), loop=loop)
	futures.append(coro)

	future = asyncio.gather(*futures)
	loop.add_signal_handler(signal.SIGINT, shutdown, future, loop)
	loop.add_signal_handler(signal.SIGTERM, shutdown, future, loop)

	loop.run_forever()
